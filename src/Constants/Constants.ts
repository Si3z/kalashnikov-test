export const BOOL_STRING = {
  TRUE: "Да",
  FALSE: "Нет"
};

export const SIZE = {
  M: "M",
  L: "L",
  XL: "XL",
  XXL: "XXL",
  XXXL: "XXXL"
};

export const TYPE = {
  BODY: "Верхняя одежда",
  UNDER: "Нижнее бельё",
  SHOES: "Обувь"
};
