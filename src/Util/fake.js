/**
 *  Не является частью основного проекта
 *  и оставлен лишь в целях ознакомления
 *  с используемым методом генерации
 *  списка продукции.
 */

const fs = require("fs");
const faker = require("faker");
const path = require("path");

var len = 1000;
var sizes = ["M", "L", "XL", "XXL", "XXXL"];
var types = ["Верхняя одежда", "Нижнее бельё", "Обувь"];
var colors = ["00", "FF", "99"];

var now = new Date();

var data = [];
faker.locale = "ru";

/* Используется вместо встроенного в faker
 * метода color, т.к. цвета получаются
 * слишком рандомными
 */
function randomColor() {
  return (
    "#" +
    colors[faker.random.number(colors.length - 1)] +
    colors[faker.random.number(colors.length - 1)] +
    colors[faker.random.number(colors.length - 1)]
  );
}

for (var id = 0; id < len; id++) {
  let inStock = faker.random.boolean();
  let obj = {
    id,
    inStock,
    color: randomColor(),
    name: faker.commerce.productName(),
    type: types[faker.random.number(types.length - 1)],
    size: sizes[faker.random.number(sizes.length - 1)],
    date: inStock ? faker.date.recent() : faker.date.future(0.5, now)
  };
  data.push(obj);
}

fs.writeFileSync(
  path.resolve(__dirname, "../Constants/clothes.json"),
  JSON.stringify(data)
);
