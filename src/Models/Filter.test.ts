import Filter from "./Filter";
import Product from "./Product";
import { TYPE, SIZE } from "../Constants/Constants";

describe("Filter model", () => {
  const product = Product.create({
    id: 231,
    name: "Уникальная Бетонная Куртка",
    type: TYPE.BODY,
    size: SIZE.XXL,
    color: "#FF0000",
    inStock: true,
    date: new Date(0)
  });
  const filter = Filter.create({
    key: "name",
    value: "",
    type: "input",
    description: "description"
  });

  beforeEach(() => {
    filter.reset();
  });

  it("Inits normally", () => {
    expect(filter).toMatchSnapshot();
  });

  it("Updates normally", () => {
    let newName = "Some new name";
    filter.update(newName);
    expect(filter.value).toBe(newName);
  });

  it("Returns true with correct name", () => {
    filter.update("куртка");
    expect(filter.apply(product)).toBeTruthy();
  });

  it("Returns false with incorrect name", () => {
    filter.update("стол");
    expect(filter.apply(product)).toBeFalsy();
  });
});
