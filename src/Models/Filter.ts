import { types, Instance, getParent } from "mobx-state-tree";
import ProductModel from "./Product";
import { BOOL_STRING } from "../Constants/Constants";

const FilterModel = types
  .model({
    key: types.enumeration(Object.keys(ProductModel.properties)),
    description: types.string,
    type: types.enumeration(["input", "select", "boolean", "date", "color"]),
    value: types.maybeNull(
      types.union(
        types.string,
        types.number,
        types.boolean,
        types.array(types.maybe(types.Date))
      )
    ),
    possibleValues: types.maybe(types.array(types.string))
  })
  .actions(self => ({
    remove() {
      getParent(self, 2).removeChild(self);
    },
    reset() {
      if (Array.isArray(self.value)) self.value = [undefined, undefined];
      else self.value = null;
    },
    update(value: any) {
      if (self.type === "date") {
        if (Array.isArray(value) && value.length === 2)
          self.value = [value[0].toDate(), value[1].toDate()];
        else self.value = [undefined, undefined];
      } else self.value = value;
    }
  }))
  .views(self => ({
    apply(product: Instance<typeof ProductModel.Type>) {
      if (
        product.hasOwnProperty(self.key) &&
        self.value !== null &&
        self.value !== undefined &&
        self.value !== "Не важно" &&
        self.value !== ""
      ) {
        let prop = product[self.key as keyof typeof ProductModel.properties];
        switch (self.type) {
          case "date": {
            if (
              Array.isArray(self.value) &&
              self.value[0] !== undefined &&
              self.value[1] !== undefined
            ) {
              if (prop <= self.value[0] || prop >= self.value[1]) return false;
            }
            return true;
          }
          case "input": {
            if (
              String(prop)
                .toLowerCase()
                .indexOf(String(self.value).toLowerCase()) === -1
            )
              return false;
            return true;
          }
          case "boolean": {
            if (
              (self.value === BOOL_STRING.TRUE && prop) ||
              (self.value === BOOL_STRING.FALSE && !prop)
            )
              return true;
            return false;
          }
          default: {
            if (String(prop).toLowerCase() !== String(self.value).toLowerCase())
              return false;
          }
        }
      }
      return true;
    }
  }));

export default FilterModel;
