import Product from "./Product";
import { TYPE, SIZE } from "../Constants/Constants";

describe("Product model", () => {
  it("Creates normally", () => {
    expect(
      Product.create({
        id: 231,
        name: "Уникальная Бетонная Куртка",
        type: TYPE.BODY,
        size: SIZE.XXL,
        color: "#FF0000",
        inStock: true,
        date: new Date(0)
      })
    ).toMatchSnapshot();
  });
});
