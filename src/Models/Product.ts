import { types } from "mobx-state-tree";
import { TYPE, SIZE } from "../Constants/Constants";

const ProductModel = types.model("Product", {
  id: types.number,
  name: types.string,
  type: types.enumeration(Object.values(TYPE)),
  size: types.enumeration(Object.values(SIZE)),
  color: types.string,
  inStock: types.boolean,
  date: types.Date
});

export default ProductModel;
