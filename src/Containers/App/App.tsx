import React from "react";
import ProductTable from "../ProductTable/ProductTable";
import Filters from "../Filters/Filters";

import "./App.css";

const App: React.FC = () => {
  return (
    <div className="app">
      <h1>Склад продукции</h1>
      <div className="app-container">
        <ProductTable />
        <Filters />
      </div>
    </div>
  );
};

export default App;
