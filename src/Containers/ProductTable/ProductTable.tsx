import React from "react";
import styles from "./producttable.module.css";
import { Table, Column, AutoSizer } from "react-virtualized";
import { useRootStore } from "../../Hooks/Store";
import { observer } from "mobx-react-lite";

function colorRenderer(cellData: any) {
  return (
    <div
      className={styles.color}
      style={{ backgroundColor: String(cellData) }}
    />
  );
}

function inStockRenderer(cellData: any) {
  if (Boolean(cellData)) return "Да";
  return "Нет";
}

function dateRenderer(cellData: any) {
  return new Date(cellData).toISOString().slice(0, 10);
}

const ProductTable: React.FC = observer(() => {
  const productList = useRootStore(store => store.productList);
  return (
    <div className={styles.table}>
      <h2>Таблица</h2>
      {!productList.length && (
        <h3 className={styles.error}>
          Не найдено продукции, соответствующей заданным критериям :с
        </h3>
      )}
      {productList.length > 0 && (
        <AutoSizer>
          {({ width }) => (
            <Table
              height={560}
              headerHeight={40}
              rowHeight={40}
              rowCount={productList.length}
              width={width}
              rowGetter={({ index }) => productList[index]}
            >
              <Column label="ID" dataKey="id" width={10} flexGrow={1} />
              <Column
                label="Наименование"
                dataKey="name"
                width={200}
                flexGrow={3}
              />
              <Column label="Тип" dataKey="type" width={100} flexGrow={1} />
              <Column label="Размер" dataKey="size" width={60} flexGrow={1} />
              <Column
                label="Цвет"
                dataKey="color"
                width={40}
                flexGrow={1}
                cellRenderer={({ cellData }) => colorRenderer(cellData)}
              />
              <Column
                label="В наличии"
                dataKey="inStock"
                width={80}
                flexGrow={1}
                cellRenderer={({ cellData }) => inStockRenderer(cellData)}
              />
              <Column
                label="Дата поступления"
                dataKey="date"
                width={100}
                flexGrow={2}
                cellRenderer={({ cellData }) => dateRenderer(cellData)}
              />
            </Table>
          )}
        </AutoSizer>
      )}
    </div>
  );
});

export default ProductTable;
