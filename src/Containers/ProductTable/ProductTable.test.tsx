import React from "react";
import { shallow } from "enzyme";
import ProductTable from "./ProductTable";

describe("Product table", () => {
  it("Renders normally", () => {
    const productTable = shallow(<ProductTable />);
    expect(productTable).toMatchSnapshot();
  });
});
