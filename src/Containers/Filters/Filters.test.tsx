import React from "react";
import { shallow } from "enzyme";
import Filters from "./Filters";

describe("Filters panel", () => {
  it("Renders normally", () => {
    const productTable = shallow(<Filters />);
    expect(productTable).toMatchSnapshot();
  });
});
