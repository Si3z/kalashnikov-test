import React from "react";
import { Button } from "antd";
import styles from "./filters.module.css";
import Filter from "../../Components/Filter/Filter";
import { useRootStore } from "../../Hooks/Store";

const Filters: React.FC = () => {
  const { filters, update, reset } = useRootStore(store => ({
    filters: store.filters,
    update: store.updateFilter,
    reset: store.resetFilters
  }));
  return (
    <div className={styles.filters}>
      <h2>Фильтры</h2>
      <div className={styles.panel}>
        {filters.map((filter, id) => {
          return <Filter filter={filter} id={id} handler={update} key={id} />;
        })}
        <Button
          type="danger"
          onClick={e => {
            reset();
          }}
        >
          Сбросить фильтры
        </Button>
      </div>
    </div>
  );
};

export default Filters;
