import {
  types,
  Instance,
  destroy,
  IAnyStateTreeNode,
  SnapshotIn
} from "mobx-state-tree";
import ProductModel from "../Models/Product";
import FilterModel from "../Models/Filter";

import productsJSON from "../Constants/clothes.json";
import { TYPE, SIZE, BOOL_STRING } from "../Constants/Constants";

const RootStoreModel = types
  .model({
    products: types.array(ProductModel),
    filters: types.array(FilterModel)
  })
  .actions(self => ({
    createFilter(snapshot: SnapshotIn<typeof FilterModel>) {
      self.filters.push(FilterModel.create(snapshot));
    },
    removeChild(target: IAnyStateTreeNode) {
      destroy(target);
    },
    updateFilter(id: number, value: any) {
      if (id >= 0 && id < self.filters.length) self.filters[id].update(value);
    },
    resetFilters() {
      self.filters.forEach(f => {
        f.reset();
      });
    }
  }))
  .views(self => ({
    get productList() {
      let list = self.products.filter(product => {
        let result = true;
        self.filters.forEach(filter => {
          if (!filter.apply(product)) {
            result = false;
          }
        });
        return result;
      });
      return list;
    }
  }));

// Загрузка товаров
let products: Instance<typeof ProductModel.Type>[] = [];
export let colors: string[] = [];

productsJSON.forEach(p => {
  products.push({ ...p, date: new Date(p.date) });
  if (colors.indexOf(p.color) === -1) colors.push(p.color);
});

// Инициализация фильтров
let filters: Instance<typeof FilterModel.Type>[] = [];

/* 
  Не смог автоматизировать создание фильтров
  по причине невозможности вытащить string literals
  из типов в ProductModel (перепробовал всё что смог нагуглить),
  а так же из за невозможности точно определить
  необходимый тип фильтра и его описание.

  Соответственно, вручную забил несколько фильтров для
  примера работоспособности системы 
*/

filters.push(
  FilterModel.create({
    key: "name",
    description: "Наименование",
    type: "input",
    value: ""
  })
);

filters.push(
  FilterModel.create({
    key: "type",
    description: "Тип продукта",
    type: "select",
    value: undefined,
    possibleValues: Object.values(TYPE)
  })
);

filters.push(
  FilterModel.create({
    key: "size",
    description: "Размер",
    type: "select",
    value: undefined,
    possibleValues: Object.values(SIZE)
  })
);

filters.push(
  FilterModel.create({
    key: "color",
    description: "Цвет",
    type: "color",
    value: "",
    possibleValues: colors
  })
);

filters.push(
  FilterModel.create({
    key: "inStock",
    description: "В наличии",
    type: "boolean",
    value: "",
    possibleValues: Object.values(BOOL_STRING)
  })
);

filters.push(
  FilterModel.create({
    key: "date",
    description: "Дата поступления",
    type: "date",
    value: undefined
  })
);

const RootStore = RootStoreModel.create({
  products,
  filters
});

export default RootStore;
