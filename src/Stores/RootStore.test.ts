import RootStore, { colors } from "./RootStore";

describe("RootStore", () => {
  it("Creates normally", () => {
    expect(RootStore).toMatchSnapshot();
  });

  it("Returns full product list if unfiltered", () => {
    expect(RootStore.productList).toMatchSnapshot();
  });

  it("Returns filtered product list", () => {
    RootStore.createFilter({
      key: "name",
      type: "input",
      value: "",
      description: "Наименование"
    });
    RootStore.updateFilter(0, "стол");
    expect(RootStore.productList).toMatchSnapshot();
  });
});

describe("Colors array", () => {
  it("Fills normally", () => {
    expect(colors).toMatchSnapshot();
  });
});
