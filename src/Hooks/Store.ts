import { useContext } from "react";
import { MSTContext } from "../context";
import RootStore from "../Stores/RootStore";

// Simple root store hook
export const useRootStore = <Selection>(
  dataSelector: (store: typeof RootStore) => Selection
) => {
  const context = useContext(MSTContext);
  if (!context) throw new Error("Error finding context");
  return dataSelector(context);
};
