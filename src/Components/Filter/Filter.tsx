import React, { Fragment } from "react";
import FilterModel from "../../Models/Filter";
import { Instance } from "mobx-state-tree";
import { Input, Select, DatePicker } from "antd";
import styles from "./filter.module.css";
import moment, { Moment } from "moment";
import { observer } from "mobx-react-lite";
const Option = Select.Option;

function dateToFieldValue(value: any): [Moment, Moment] | [] {
  return Array.isArray(value) &&
    value[0] !== undefined &&
    value[1] !== undefined
    ? [moment(value[0]), moment(value[1])]
    : [];
}

interface IFilter {
  filter: Instance<typeof FilterModel>;
  id: number;
  handler: (id: number, value?: any) => void;
}

const Filter: React.FC<IFilter> = observer(({ filter, id, handler }) => {
  return (
    <div className="filter">
      <label>{filter.description}</label>
      {filter.type === "input" && (
        <Input
          type="text"
          value={typeof filter.value === "string" ? filter.value : ""}
          onChange={e => handler(id, e.target.value)}
        />
      )}
      {["select", "color", "boolean"].indexOf(filter.type) !== -1 && (
        <Select
          value={filter.value ? filter.value.toString() : ""}
          onChange={(data: string) => handler(id, data)}
          style={{ display: "block" }}
        >
          <Option value="">Не важно</Option>
          {Array.isArray(filter.possibleValues) &&
            filter.possibleValues.map((value: string) => {
              return (
                <Option value={value} key={value}>
                  {["select", "boolean"].indexOf(filter.type) !== -1 && value}
                  {filter.type === "color" && (
                    <Fragment>
                      <div
                        className={styles.colorbox}
                        style={{ backgroundColor: value }}
                      ></div>
                      <div className={styles.color} style={{ color: value }}>
                        {value}
                      </div>
                    </Fragment>
                  )}
                </Option>
              );
            })}
        </Select>
      )}
      {filter.type === "date" && (
        <DatePicker.RangePicker
          value={dateToFieldValue(filter.value)}
          onChange={range => handler(id, range)}
        ></DatePicker.RangePicker>
      )}
    </div>
  );
});

export default Filter;
