import React from "react";
import { shallow } from "enzyme";
import Filter from "./Filter";
import FilterModel from "../../Models/Filter";
import { SIZE } from "../../Constants/Constants";

describe("Filters panel", () => {
  it("Input filter renders normally", () => {
    const props = {
      filter: FilterModel.create({
        key: "name",
        description: "Наименование",
        value: "",
        type: "input"
      }),
      id: 0,
      handler: jest.fn()
    };
    const FilterField = shallow(<Filter {...props} />);
    expect(FilterField).toMatchSnapshot();
  });

  it("Select filter renders normally", () => {
    const props = {
      filter: FilterModel.create({
        key: "size",
        description: "Размер",
        value: "",
        type: "select",
        possibleValues: Object.values(SIZE)
      }),
      id: 0,
      handler: jest.fn()
    };
    const FilterField = shallow(<Filter {...props} />);
    expect(FilterField).toMatchSnapshot();
  });

  it("Date filter renders normally", () => {
    const props = {
      filter: FilterModel.create({
        key: "name",
        description: "Наименование",
        value: "",
        type: "date"
      }),
      id: 0,
      handler: jest.fn()
    };
    const FilterField = shallow(<Filter {...props} />);
    expect(FilterField).toMatchSnapshot();
  });
});
