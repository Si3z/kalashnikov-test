import React, { createContext } from "react";
import RootStore from "./Stores/RootStore";

export const MSTContext = createContext(RootStore);

export const StoreProvider: React.FC = ({ children }) => {
  return (
    <MSTContext.Provider value={RootStore}>{children}</MSTContext.Provider>
  );
};
